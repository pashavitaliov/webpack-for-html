const NODE_ENV = process.env.NODE_ENV || 'development';
const webpack = require('webpack');
const path = require('path');
const ExtractTextPlugin = require("extract-text-webpack-plugin");
const HtmlWebpackPlugin = require('html-webpack-plugin');

switch (NODE_ENV){
  case 'production': module.exports = {

    context:  __dirname + '/src',
    entry: {
      home: './js/script'
    },

    output: {
      path:  path.resolve(__dirname, 'dist/assets/js'),
      filename: '[name].js',
      library: '[name]'
    },

    module: {
      rules: [{
        test: /\.js&/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
          options: {
            extends: path.join(__dirname, '.babelrc')
          }
        }
      },{
        test: /\.css?$/,
        include: path.resolve(__dirname, './src/css'),
        use: ExtractTextPlugin.extract({
          use: [
            {
              loader: 'css-loader',
              options: {
                minimize: true,
                sourceMap: true
              }
            },
            { loader: 'autoprefixer-loader?browsers=last 4 version' },
          ]
        })
      },{
        test: /\.(png|jpg|svg|ttf|eot|woof|woof2)$/,
        use: [{
          loader: 'file-loader',
          options: {
            name: '[path][name].[ext]',
            outputPath: '../'
          }
        }]
      }]
    },

    plugins: [
      new ExtractTextPlugin('../css/style.css', { allChunks: true}),
      // new webpack.ProvidePlugin({
      //   $: 'jquery'
      // }),
      // new webpack.NoEmitOnErrorsPlugin(),
      new webpack.DefinePlugin({
        NODE_ENV: JSON.stringify(NODE_ENV)
      }),
      new webpack.optimize.UglifyJsPlugin({
        warnings: false,
        drop_console: true,
        unsafe: true
      })
    ]
  };
  break;

  case 'development': module.exports = {
      context:  __dirname + '/src',
      entry: {
        home: './js/script'
      },

      output: {
        path:  __dirname + 'dist',
        filename: '[name].js',
        library: '[name]'
      },

      devtool: 'inline-cheap-module-source-map',

      module: {
        rules: [{
          test: /\.js&/,
          exclude: /node_modules/,
          use: {
            loader: 'babel-loader',
            options: {
              extends: path.join(__dirname, '.babelrc')
            }
          }
        },{
          test: /\.css$/,
          use: ['style-loader', 'css-loader', 'autoprefixer-loader?browsers=last 4 version']
        },{
          test: /\.(png|jpg|svg|ttf|eot|woof|woof2)$/,
          use: [{
            loader: 'file-loader',
            options: {
              name: '[path][name].[ext]'
            }
          }]
        }]
      },

      devServer: {
        contentBase: __dirname + '/dist',
        host: 'localhost',
        port: 8080,
        hot: true
      },


      plugins: [
        new webpack.NamedModulesPlugin(),
        new webpack.HotModuleReplacementPlugin(),
        // new webpack.ProvidePlugin({
        //   $: 'jquery'
        // }),
        // new webpack.NoEmitOnErrorsPlugin(),
        new webpack.DefinePlugin({
          NODE_ENV: JSON.stringify(NODE_ENV)
        })
      ]
    };
  break;

  default:
    throw new Error("Error in NODE_ENV");
}

/*

module.exports = {
  context:  __dirname + '/src',
  entry: {
    home: './js/home'
  },

  output: {
    path:  path.resolve(__dirname, 'dist/js'),
    filename: '[name].js',
    library: '[name]'
  },

  devtool: NODE_ENV === 'development' ? 'inline-cheap-module-source-map' : false,

  module: {
    rules: [{
      test: /\.js&/,
      exclude: /node_modules/,
      use: {
        loader: 'babel-loader',
        options: {
          extends: path.join(__dirname, '.babelrc')
        }
      }
    }, {
      test: /\.(png|jpg|svg|ttf|eot|woof|woof2)$/,
      use: [{
        loader: 'file-loader',
        options: {
          name: '[path]/[name].[ext]',
          outputPath: '../'
        }
      }]
    }]
  },

  devServer: {
    contentBase: __dirname + '/dist',
    host: 'localhost',
    port: 8080,
    hot: true
  },


  plugins: [
    new webpack.NamedModulesPlugin(),
    new webpack.HotModuleReplacementPlugin(),
    new ExtractTextPlugin('../css/style.css', { allChunks: true, disable: NODE_ENV === 'development' }),
    new webpack.ProvidePlugin({
      $: 'jquery'
    }),
    // new webpack.NoEmitOnErrorsPlugin(),
    new webpack.DefinePlugin({
      NODE_ENV: JSON.stringify(NODE_ENV)
    })
  ]
};


if ( NODE_ENV === 'production' ) {
  module.exports.plugins.push(
      new webpack.optimize.UglifyJsPlugin({
        warnings: false,
        drop_console: true,
        unsafe: true
      })
  );

}

if ( NODE_ENV === 'development' ) {
  module.exports.module.rules.push({
    test: /\.css$/,
    use: ['style-loader', 'css-loader', 'autoprefixer-loader?browsers=last 4 version']
  });
}

if ( NODE_ENV === 'production' ){
  module.exports.module.rules.push({
    test: /\.css?$/,
    include: path.resolve(__dirname, './src/css'),
    use: ExtractTextPlugin.extract({
      use: [
        {
          loader: 'css-loader',
          options: {
            minimize: true,
            sourceMap: true
          }
        },
        { loader: 'autoprefixer-loader?browsers=last 4 version' },
      ]
    })
  });
}


*/
