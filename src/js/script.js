import '../css/style.css'

$(document).ready(function() {

  $('.nav__burger').on('click', function() {
    $('.header__menu').toggleClass('open');
  });

  $(document).on('scroll', function() {
    if ( $(this).scrollTop() > 60 ) {
      $('.header').addClass('scroll');
      $('.header-container').addClass('scroll');

      $('.header').animate({ top: 0 }, 10);

      $('.page-banner').addClass('scroll')
    }
    if ( $(this).scrollTop() == 0 ) {
      $('.header').removeClass('scroll');
      $('.header').css('top', '-=60');
      $('.page-banner').removeClass('scroll')
      $('.header-container').removeClass('scroll')

    }
  });
});